###################
Codeigniter 3 + API Kawal Corona
###################

Berikut adalah Source code penggunaan API Kawal Corona menggunakan Codeigniter 3.

*******************
Requirements
*******************

- Codeigniter 3.1.11
- PHP 7.2

*******************
Video Tutorial
*******************
- `Bad Practise Channel <https://www.youtube.com/playlist?list=PLfF5VnUxGQsQe2tK9fi6HIOMhMKdaGU_k>`_

*******************
Resource
*******************
- `Kawal Corona <https://kawalcorona.com/>`_
